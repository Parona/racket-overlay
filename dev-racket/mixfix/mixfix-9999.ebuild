# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="sorawee/mixfix"

inherit racket gh

DESCRIPTION="Mixfix for Racket"
HOMEPAGE="https://github.com/sorawee/mixfix"

LICENSE="all-rights-reserved"
SLOT="0"
RESTRICT="mirror"
