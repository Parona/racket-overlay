# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="Kalimehtar/russian-lang"
GH_COMMIT="08d498aa0a4a4f8ccab84892180153cf20966a16"

inherit racket gh

DESCRIPTION="Programming language based on russian language"
HOMEPAGE="https://github.com/Kalimehtar/russian-lang"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
