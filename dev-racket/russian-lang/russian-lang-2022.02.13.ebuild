# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="Kalimehtar/russian-lang"
GH_COMMIT="e6ece421b1f8d1d531555fda928473e32afa606b"

inherit racket gh

DESCRIPTION="Programming language based on russian language"
HOMEPAGE="https://github.com/Kalimehtar/russian-lang"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
