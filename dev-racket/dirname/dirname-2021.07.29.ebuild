# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="gitlab.com"
GH_REPO="xgqt/racket-dirname"
GH_COMMIT="955e1f1f58c6221006922e8a6f0b3aed7722f572"

inherit racket gh

DESCRIPTION="Basename and dirname functions"
HOMEPAGE="https://gitlab.com/xgqt/racket-dirname"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
