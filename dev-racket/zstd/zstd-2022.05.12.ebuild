# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="git.sr.ht"
GH_REPO="~williewillus/racket-zstd"
GH_COMMIT="149c251507fcce5206091841089f00924a9c15df"

inherit racket gh

DESCRIPTION="the zstd Racket package"
HOMEPAGE="https://git.sr.ht/~williewillus/racket-zstd"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
