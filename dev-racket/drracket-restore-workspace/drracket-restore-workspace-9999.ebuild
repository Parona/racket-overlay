# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="sorawee/drracket-restore-workspace"

inherit racket gh

DESCRIPTION="Restore workspace for DrRacket"
HOMEPAGE="https://github.com/sorawee/drracket-restore-workspace"

LICENSE="all-rights-reserved"
SLOT="0"
RESTRICT="mirror"
