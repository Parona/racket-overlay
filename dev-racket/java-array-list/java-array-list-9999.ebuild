# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="johnnyodonnell/java-array-list"

inherit racket gh

DESCRIPTION="Clone of Java's ArrayList"
HOMEPAGE="https://github.com/johnnyodonnell/java-array-list"

LICENSE="all-rights-reserved"
SLOT="0"
RESTRICT="mirror"
