# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="AlexKnauth/typed-racket-stream"
GH_COMMIT="45fe2ae51511cf10caf6fcc6383cce5474e05122"

inherit racket gh

DESCRIPTION="Streams for typed racket"
HOMEPAGE="https://github.com/AlexKnauth/typed-racket-stream"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
