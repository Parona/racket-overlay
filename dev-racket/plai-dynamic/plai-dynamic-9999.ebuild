# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="pivot.cs.unb.ca"
GH_REPO="git/plai-dynamic"

inherit racket gh

DESCRIPTION="the plai-dynamic Racket package"
HOMEPAGE="https://pivot.cs.unb.ca/git/plai-dynamic"

LICENSE="all-rights-reserved"
SLOT="0"
RESTRICT="mirror"
