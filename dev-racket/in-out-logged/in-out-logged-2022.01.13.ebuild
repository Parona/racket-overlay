# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="dstorrs/in-out-logged"
GH_COMMIT="66e74e0ab7fbbd9f4e07e5c9257ea8552bf7821b"

inherit racket gh

DESCRIPTION="the in-out-logged Racket package"
HOMEPAGE="https://github.com/dstorrs/in-out-logged"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
