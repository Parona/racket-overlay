# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="gitlab.com"
GH_REPO="xgqt/racket-mike"
GH_COMMIT="aeaf11d42423feedfabdf4bac847236324a5a36f"

inherit racket gh

DESCRIPTION="Micro Make replacement"
HOMEPAGE="https://gitlab.com/xgqt/racket-mike"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
