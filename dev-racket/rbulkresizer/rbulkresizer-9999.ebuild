# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="gitlab.com"
GH_REPO="xgqt/rbulkresizer"

inherit racket gh

DESCRIPTION="Graphical bulk picture resize tool"
HOMEPAGE="https://gitlab.com/xgqt/rbulkresizer"

LICENSE="all-rights-reserved"
SLOT="0"
RESTRICT="mirror"
