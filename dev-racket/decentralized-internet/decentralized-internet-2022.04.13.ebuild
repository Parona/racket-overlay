# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="Lonero-Team/Racket-Package"
GH_COMMIT="8e9e60ed7072918da3162383d079598021d06adb"

inherit racket gh

DESCRIPTION="A package for building distributed computing projects"
HOMEPAGE="https://github.com/Lonero-Team/Racket-Package"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
