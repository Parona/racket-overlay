# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="sorawee/define-who"

inherit racket gh

DESCRIPTION="An anaphoric macro that binds who to function name"
HOMEPAGE="https://github.com/sorawee/define-who"

LICENSE="all-rights-reserved"
SLOT="0"
RESTRICT="mirror"
