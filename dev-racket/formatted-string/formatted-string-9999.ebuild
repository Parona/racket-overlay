# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="dannypsnl/formatted-string"

inherit racket gh

DESCRIPTION="Extends racket string to formatted string"
HOMEPAGE="https://github.com/dannypsnl/formatted-string"

LICENSE="all-rights-reserved"
SLOT="0"
RESTRICT="mirror"
