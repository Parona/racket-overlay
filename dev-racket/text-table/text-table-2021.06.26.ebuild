# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

GH_DOM="github.com"
GH_REPO="Metaxal/text-table"
GH_COMMIT="f4981c379e376b415509c8939a389e3aab49353b"

inherit racket gh

DESCRIPTION="A simple package to display text tables with utf-8 frames."
HOMEPAGE="https://github.com/Metaxal/text-table"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"
